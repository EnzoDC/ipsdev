# Résolution des équations d'un oscillateur harmonique quantique 1D

Instructions de build :
- Installer des versions récentes de **doxygen**, **astyle**, **cxxtest**, **armadillo**, **graphviz**, **g++**
- Lancer la commande `make`
- La doc est générée dans **Doxygen/**, l'éxécutable principal dans **bin/main**, et les tests dans **bin/tests**