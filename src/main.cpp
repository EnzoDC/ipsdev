#include <iostream>
#include <armadillo>
#include "../include/solver.h"
#include "../include/const.h"
#include "../include/orthonormality.h"

int main() {
	Solver solver(n, z, m, w);
	solver.solve();
	solver.psi.print();
	solver.psi.save("data.csv", arma::csv_ascii);
	z.save("zaxis.csv", arma::csv_ascii);
	/*orthonormalityQuadrature(22, 22);*/
}
