#include <math.h>
#include <armadillo>

#include "../include/const.h"
#include "../include/gauss_hermite.h"

double gauss_hermite(arma::rowvec (*func) (arma::rowvec)) {
	arma::rowvec nodes = hermgaussNodes;
	arma::rowvec weights = hermgaussWeights;
	return arma::accu(weights % func(nodes));
}