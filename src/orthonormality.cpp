#include <math.h>

#include "../include/orthonormality.h"
#include "../include/const.h"
#include "../include/hermite.h"

/**
 * @file orthonormality.h
 *
 * @brief this file contains a function to resolve
 */


/**
 * @brief function that calculates the integral of \f$\psi^*_p(z)\psi_q(z)\f$ using
 * Gaussian gauss quadrature using hermite-gauss weight function
 *
 * @param q the degree of the first \f$\psi\f$ solution
 * @param p the degree of the second \f$\psi\f$ solution
 * @return double
 *
 * @details the \f$\psi\f$s are defined as such :
 * \f$\psi_n(z) = \frac{1}{\sqrt{2^n n!}}\left(\frac{m\omega}{\pi\hbar}\right)^{1/4}e^{-\frac{m\omega z^2}{2\hbar}}H_n\left(\sqrt{\frac{m\omega}{\hbar}} . z\right)\f$
 * the function returns the result of \f$ \int \psi^*_p(z)\psi_q(z) dz\f$
 * result is expected to be \f$ \delta_{pq} \f$.
 * @see hermite.cpp
 * @note The aim is to calculate \f$\int_{-\infty}^{+\infty}\psi_p(z)\psi_q(z)dz\f$\n
 * With : \f$\forall z, \psi_q(z)\psi_p(z) = \frac{1}{\sqrt{2^{p+q}p!q!}}\sqrt{\frac{m\omega}{\pi\hbar}}\exp{-\frac{m\omega z^2}{\hbar}}H_p(\sqrt{\frac{m\omega}{\hbar}}z)H_q(\sqrt{\frac{m\omega}{\hbar}}z)\f$\n
 * Using \f$ x = \sqrt{\frac{m\omega}{\hbar}}z\f$, we have \f$ dx = \sqrt{\frac{m\omega}{\hbar}}dz\f$\n
 * Thus, our integral becomes : \f$\int_{-\infty}^{+\infty}\frac{1}{\sqrt{2^{p+q}p!q!\pi}}\exp{-x^2}H_p(x)H_q(x)dx\f$\n
 * Which removes the need to use \f$ m\f$ and \f$ \omega\f$, and some floating point errors.
 */
double orthonormalityQuadrature(unsigned int p, unsigned int q) {
	/*How to calculate this thing :
	    1. check p+q < 2 times the length of the arma::rowvec hermgaussNodes
	    2. generate Hermite for max(p,q) with hermgaussNodes
	    3. take the arma::rowvec of Hermite_p and Hermite_q
	    4. multiply them by themself (element per element)
	    5. multiply it element per element with hermgaussWeights
	    6. sum it all
	    7. multiply the result by 1/sqrt(2^(n+m)*q!*p!*pi)
	    8. enjoy a (very probably) false result qwq (should get a nice approx of int(p==q))
	*/

	/*1*/
	if (p+q >= 2*hermgaussNodes.n_elem) {
		throw std::invalid_argument("p+q should be lesser than 2*hermgaussNodes.n_elem");
	}

	/*2*/
	int maxpq = std::max(p,q);
	int minpq = std::min(p,q);
	Hermite hermite(maxpq+1, hermgaussNodes);
	/*3*/
	arma::rowvec pVec = hermite.H.row(p);
	arma::rowvec qVec = hermite.H.row(q);
	/*4 5*/
	arma::rowvec res = pVec % qVec;
	res = hermgaussWeights % res;
	/*6*/
	double sum = arma::accu(res);

	//res.print(); //debug
	//std::cout << "the sum is before mult " << sum << "\n"; debug
	/*7*/
	int facto_pq = 1;/* p!q! */
	for (int i=1; i<maxpq+1; i++) {
		if (i <= minpq) {
			facto_pq=facto_pq*i*i;
		} else {
			facto_pq=facto_pq*i;
		}
	}
	//std::cout << "the factorial " << facto_pq << "\n"; //debug
	double cst = 1/(sqrt(pow(2.0, (float) p+q)*facto_pq*M_PI));

	//std::cout << "the result " << cst*sum << "\n"; //debug
	return cst*sum;
};