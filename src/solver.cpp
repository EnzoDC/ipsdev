#include <math.h>

#include "../include/solver.h"
#include "../include/const.h"
#include "../include/hermite.h"

/**
 * @brief Construct a new Solver with the given number of harmonics, points, mass and frequency
 *
 * @param _n the number of harmonics desired
 * @param _z the points to evaluate
 * @param _m mass of the object
 * @param _w frequency of the oscillator
 */
Solver::Solver(int _n, arma::rowvec _z, double _m, double _w) {
	n = _n;
	z = _z;
	m = _m;
	w = _w;
	factor = m * w / planck;
	H = new Hermite(n, sqrt(factor) * z);
}

/**
 * @brief calculates the solutions of the 1D-HO and stores them in a matrix for all n (rows) and z (columns)
 *
 * @details First the function calculates the vectors factorials and exponentials, which respectively
 * contains the factors depending only on n and only on z.
 * Those vectors are then multiplied to make a matrix, which is itself multiplied (element-wise) to H.
 */
void Solver::solve() {
	// Calculation of 1/sqrt(2^n * n!)
	arma::vec factorials(n);
	factorials(0) = 1;
	for (int i = 1; i < n; i++) {
		factorials(i) = factorials(i - 1) * 1 / sqrt(2 * (i + 1));
	}
	// Calculation of e^(-mwz²/2h)
	arma::rowvec exponentials(z.n_elem);
	exponentials = arma::exp((-factor / 2) * arma::pow(z, 2));
	// Calculation of psi_n
	psi = arma::mat(n, z.n_elem);
	psi = (factorials * exponentials) % H->H  * pow(factor / M_PI, 1 / 4);
}

Solver::~Solver() {
	delete (H);
}

