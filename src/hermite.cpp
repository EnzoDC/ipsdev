#include "../include/hermite.h"


/**
 * @brief calculates a matrix containing the values of H_n(z) for all z (columns) and n (rows)
 * @details The matrix is calculated row by row using the recursion formula.
 * This constructor computes H with the same values of z for each row
 */
Hermite::Hermite(int n, arma::rowvec z) {
	H = arma::mat(n, z.n_elem);
	H.row(0) = arma::rowvec(z.n_elem, arma::fill::ones);
	H.row(1) = z * 2;
	for (unsigned int i = 2; i < H.n_rows; i++) {
		H.row(i) = 2 * (z % H.row(i - 1)) - 2 * (i - 1) * H.row(i - 2);
	}
}

