#include <math.h>

#include "../include/schrodinger.h"
#include "../include/solver.h"
#include "../include/const.h"

/**
 * Verify the 1D-HO Schrödinger equation
 *
 * @param _n the number of harmonics desired
 * @param _z the points to evaluate
 * @param _m mass of the object
 * @param _w frequency of the oscillator
 */
Schrodinger::Schrodinger(int _n, arma::rowvec _z, double _m, double _w) :
	n(_n), z(_z), m(_m), w(_w) {
	S = new Solver(n, z, m, w);
	S->solve();
}

/**
 * Compute the left part of the schrodinger equation.
 * @brief compute \f$\left(\frac{\hat{p}_{(z)}^2}{2m} + \frac{1}{2}m\omega^2\hat{z}^2\right)\psi_n\f$
 *
 * @return a matrice thet represent the left part of the schrodinger equation variing on z & n.
 */
arma::mat Schrodinger::run() {
	arma::mat right = (((m * w * w) / 2) * (S->psi % S->psi));
	right = right.submat(0, 1, n - 1, z.n_elem - 2);

	arma::mat zs1 = arma::vec(n, arma::fill::ones) * z;
	arma::mat zs2 = arma::vec(n, arma::fill::ones) * z.subvec(0, z.n_cols - 2);
	arma::mat left = derive(derive(S->psi, zs1), zs2) * (-planck * planck) / (2 * m);
	arma::mat res = left + right;

	return res;
}

/**
 * Derivation function.
 *
 * @brief derive H by zs, like: (H[n,m] - H[n - 1, m]) / (zs[n,m] - zs[n - 1, m])
 * @param psi a matrice of element to derive.
 * @param zs a matrice of element we use as reference for the derivation.
 * @return a matrice of all terms derived from H by zs. The matrice os of size H.n_row, H.n_cols - 1
 */
arma::mat Schrodinger::derive(arma::mat psi, arma::mat zs) {
	arma::mat psi1 = psi.submat(0, 0, psi.n_rows - 1, psi.n_cols - 2);
	arma::mat psi2 = psi.submat(0, 1, psi.n_rows - 1, psi.n_cols - 1);

	arma::mat zs1 = zs.submat(0, 0, zs.n_rows - 1, zs.n_cols - 2);
	arma::mat zs2 = zs.submat(0, 1, zs.n_rows - 1, zs.n_cols - 1);

	arma::mat psid = psi2 - psi1;
	arma::mat zsd = zs2 - zs1;

	arma::mat res = psid / zsd;
	return res;
}

/**
 * Destructeur for Schrodinger objects.
 */
Schrodinger::~Schrodinger() {
	delete (S);
}

