#include <cxxtest/TestSuite.h>

#include "../include/hermite.h"

/**
 * @brief test if the Hermite polynomes are correct
 *
 */
class TestHermite : public CxxTest::TestSuite {
public:
	/**
	 * Test should be equal
	 *
	 */
	void testHermite (void) {
		arma::rowvec z_test = arma::rowvec( {-2, -1, 0, 1, 2} );
		Hermite hermite(5, z_test);
		TS_ASSERT(arma::all(arma::all(hermite.H == arma::mat( {
			{1, 1, 1, 1, 1},
			{-4, -2, 0, 2, 4},
			{14, 2, -2, 2, 14},
			{-40, 4, 0, -4, 40},
			{76, -20, 12, -20, 76}
		}))));
	}
};

