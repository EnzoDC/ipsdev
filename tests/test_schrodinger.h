#include <cxxtest/TestSuite.h>

#include "../include/schrodinger.h"
#include "../include/solver.h"
#include "../include/const.h"

/**
 * Test Schrodinger
 *
 * @brief test if the Solver respect the Schrodinger equation.
 *
 */
class TestSchrodinger: public CxxTest::TestSuite {
public:

	/**
	 * Test if the derivative fonction of TestSchrodinger work on a linear function.
	 */
	void testderive(void) {
		arma::rowvec z = arma::regspace<arma::rowvec>(-10, 1, 10) * 10;
		Schrodinger schro(n, z, m, w);

		arma::mat res = schro.derive(arma::rowvec({0, 2, 4, 8, 10}) * 2, arma::rowvec({0, 2, 4, 8, 10}));

		TS_ASSERT(arma::all(arma::mean(res, 1) == arma::rowvec({2})));
	}

	/**
	 * Test if the Schrodinger equation is verified.
	 */
	void testSchrodinger(void) {
		arma::rowvec z = arma::regspace<arma::rowvec>(-10, 1, 10) * 10;
		Schrodinger schro(n, z, m, w);
		Solver solver(n, arma::regspace<arma::rowvec>(-10, 1, 10) * 10e-9, m, w);
		solver.solve();

		arma::mat res = schro.run();
		(res / solver.psi.submat(0, 1, solver.psi.n_rows - 1, solver.psi.n_cols - 2)).print("\nEn:");

//		TS_ASSERT(arma::all(arma::all(sch.run() == S.psi.submat(0, 0, S.psi.n_rows - 1, S.psi.n_cols - 3) * En)));
	}
};
