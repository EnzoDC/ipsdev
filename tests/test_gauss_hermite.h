#include <cxxtest/TestSuite.h>

#include "../include/gauss_hermite.h"
#include <math.h>

arma::rowvec func_test(arma::rowvec z) {
	arma::rowvec res = arma::rowvec(z.n_elem, arma::fill::ones);
	return res;
}

/**
 * @brief test GaussHermite function
 *
 */
class TestGaussHermite : public CxxTest::TestSuite {
public:
	/**
	 * Test should be equal
	 */
	void testGaussHermite (void) {
		double integ = gauss_hermite(&func_test);
		TS_ASSERT_DELTA( integ, sqrt(M_PI), 1e-10);
	}
};