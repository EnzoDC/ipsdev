#include <cxxtest/TestSuite.h>

#include "../include/orthonormality.h"

/**
 * @brief test if the Orthonormality criteria is met
 *
 */
class TestOrthonormality : public CxxTest::TestSuite {
public:
	/**
	 * Test should be equal to 0
	 */
	void testOrthonormalityInequalFirst(void) {
		int p = 3;
		int q = 5;
		double res = orthonormalityQuadrature(p, q);
		TS_ASSERT_DELTA(res, 0, 1e-10);
	}

	/**
	 * Test should be equal to 0
	 */
	void testOrthonormalityInequalSecond(void) {
		int p = 7;
		int q = 2;
		double res = orthonormalityQuadrature(p, q);
		TS_ASSERT_DELTA(res, 0, 1e-10);
	}

	/**
	 * Test should be equal to 1
	 */
	void testOrthonormalityEqualFirst(void) {
		int p = 3;
		int q = 3;
		double res = orthonormalityQuadrature(p, q);
		TS_ASSERT_DELTA(res, 1, 1e-10);
	}

	/**
	 * Test should be equal to 1
	 */
	void testOrthonormalityEqualSecond(void) {
		int p = 5;
		int q = 5;
		double res = orthonormalityQuadrature(p, q);
		TS_ASSERT_DELTA(res, 1, 1e-10);
	}
};