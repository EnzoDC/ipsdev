CXX = g++
CXXFLAGS = -fdiagnostics-color=always -g -O0 -Wall -Wextra -Wunreachable-code -Werror -std=c++11
# -ansi -pedantic -Wwrite-strings -Wstrict-prototypes 
CXXTEST = cxxtestgen
CXXTESTFLAGS = --error-printer

LIBS = -larmadillo

# target
TARGET = main
TESTTARGET = tests
LIB = mylib.a

# sources
SRCDIR = src
INCLUDEDIR = include
TESTDIR = tests
# bin
TARGETDIR = bin
OBJDIR = obj
LIBDIR = lib
# doc
DOCDIR = Doxygen

SRCS = $(filter-out main.cpp, $(notdir $(wildcard $(SRCDIR)/*.cpp)))
#$(warning $(SRCS))
OBJS = $(addprefix $(OBJDIR)/, $(SRCS:cpp=o))
#$(warning $(OBJS))
MAINOBJ = obj/main.o

TESTS = $(wildcard $(TESTDIR)/*.h)
#$(warning $(TESTS))
TESTSRC = obj/tests.cpp
TESTOBJ = obj/tests.o

.PHONY: all
all:
	$(MAKE) astyle
	$(MAKE) mkdirectory
	$(MAKE) $(TARGET)
	$(MAKE) $(TESTTARGET)
	$(MAKE) runtests
	$(MAKE) doc

$(TARGET): $(LIB) $(MAINOBJ)
	$(CXX) -o $(TARGETDIR)/$(TARGET) $(MAINOBJ) $(LIBDIR)/$(LIB) $(CXXFLAGS) $(LIBS)

$(TESTTARGET): $(LIB)
	$(CXXTEST) -o $(TESTSRC) $(CXXTESTFLAGS) $(TESTS)
	$(CXX) -o $(TESTOBJ) -c $(TESTSRC) $(CXXFLAGS)
	$(CXX) -o $(TARGETDIR)/$(TESTTARGET) $(TESTOBJ) $(LIBDIR)/$(LIB) $(CXXFLAGS) $(LIBS)

$(LIB): $(OBJS)
	ar crv $(LIBDIR)/$(LIB) $(OBJS)

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp
	$(CXX) -o $@ -c $< $(CXXFLAGS)


.PHONY: astyle
astyle:
	astyle --options=astyle.conf "*.cpp" "*.h"
	find ./ -name "*.orig" -exec rm -v {} \;

.PHONY: doc
doc: mkdirectory
	doxygen

.PHONY: runtests
runtests:
	./bin/tests

.PHONY: run
run:
	./bin/main

.PHONY: mkdirectory
mkdirectory:
	mkdir -p $(TARGETDIR)
	mkdir -p $(OBJDIR)
	mkdir -p $(LIBDIR)
	mkdir -p $(DOCDIR)

.PHONY: clean
clean:
	rm -rf $(TARGETDIR)
	rm -rf $(OBJDIR)
	rm -rf $(LIBDIR)
	rm -rf $(DOCDIR)

