#ifndef GAUSS_HERMITE_H
#define GAUSS_HERMITE_H
#include <armadillo>

/**
 * @brief Computes the integral from -inf to +inf of func(x) * exp(-x^2) using a Gauss-Hermite quadrature
 *
 * @param func the function to integrate
 * @return double the value of the integral
 */
double gauss_hermite(arma::rowvec (*func) (arma::rowvec));

#endif //GAUSS_HERMITE_H