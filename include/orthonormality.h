#ifndef ORTHONORMALITY_H
#define ORTHONORMALITY_H

#include <armadillo>

double orthonormalityQuadrature(unsigned int, unsigned int);

#endif
