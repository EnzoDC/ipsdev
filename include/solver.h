#ifndef SOLVER_H
#define SOLVER_H

#include <armadillo>

#include "../include/hermite.h"
/**
 * @class Solver
 *
 * @brief A solver is an object designed to solve the 1D-HO equation
 */
class Solver {
private:
	int n;
	arma::rowvec z;
	double factor;
	double m;
	double w;

public:
	Hermite *H; /*!< This matrix stores the Hermite Polynoms*/
	arma::mat psi; /*!< This matrix stores the solutions of the equation, with n in rows and z in columns*/

	Solver(int, arma::rowvec, double, double);
	~Solver();

	void solve();
};

#endif //SOLVER_H
