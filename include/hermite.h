#ifndef HERMITE_H
#define HERMITE_H

#include <armadillo>
/**
 * @class Hermite
 *
 * @brief This class evaluates the Hermite polynoms for given n ranks and a mat of z points.
 *
 */
class Hermite {
public:
	arma::mat H; /*!< This matrix stores the Hermite Polynoms*/
	Hermite(int, arma::rowvec);
};

#endif
