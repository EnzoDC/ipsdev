#ifndef INCLUDE_SCHRODINGER_H
#define INCLUDE_SCHRODINGER_H

#include "../include/solver.h"

/**
 * Schrodinger verificarions.
 *
 * @brief Implement methods for verifying Schrodinger equation.
 */
class Schrodinger {
private:
	int n;
	arma::rowvec z;
	double m;
	double w;

	/**
	 * The solver used to get PS
	 */
	Solver *S;

public:

	Schrodinger(int, arma::rowvec, double, double);
	~Schrodinger();

	arma::mat run();
	arma::mat derive(arma::mat, arma::mat);
};

#endif /* INCLUDE_SCHRODINGER_H */
