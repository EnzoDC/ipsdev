#ifndef CONST_H
#define CONST_H

#include <armadillo>
/**
 * @file const.h
 *
 * @brief this file defines various constant that will be used throughout other files.
 */

/**
 * @var extern const double planck
 * @brief Reduced Planck Constant. (unit : \f$\alpha\f$g * pm^2 / ps where 1 \f$\alpha\f$g = 10^-27 g)
 */
extern const double planck;

/**
 * @brief armadillo vector (rowvec) of the first ten hermite gauss nodes
 * @var extern const arma::rowvec hermgaussNodes
 * @details generated with numpy python library using `numpy.polynomial.hermite.hermgauss(20)`
 */
extern const arma::rowvec hermgaussNodes;

/**
 * @brief armadillo vector (rowvec) of the first ten hermite gauss weights
 * @var extern const arma::rowvec hermgaussWeights
 * @details generated with numpy python library using `numpy.polynomial.hermite.hermgauss(20)`
 */
extern const arma::rowvec hermgaussWeights;

/**
 * @brief number of energy levels desired
 *
 */
extern const int n;

/**
 * @var extern const arma::rowvec z
 * @brief Positions to evaluate. (unit : pm)
 */
extern const arma::rowvec z;

/**
 * @var extern const double m
 * @brief Neutron mass. (unit : \f$\alpha\f$g = 10^-27g)
 */
extern const double m;

/**
 * @var extern const double w
 * @brief Angular frequency. (unit : rad/ps)
 */
extern const double w;

#endif //CONST_H
